import java.util.Scanner;
public class ShutTheBox{
	
	public static void main(String args[]){
		
		Scanner sc = new Scanner(System.in);
			System.out.println("Would you like to play Shut the Box?");
			char ans = sc.nextLine().charAt(0);
		
			System.out.println("What is player1's name?");
			String player1 = sc.nextLine();
			
			System.out.println("What is player2's name?");
			String player2 = sc.nextLine();
			
			boolean winner1 = false;
			boolean winner2 = false;
			
			int wins1 = 0;
			int wins2 = 0;
			
				while(ans == 'y'){
					System.out.println("Welcome to Shut the Box");
					Board brd = new Board();
					boolean gameOver = false;
		
						while (gameOver != true) {
			
							System.out.println("=-=-=-=-=-=-=-=");
			
							System.out.println(player1+"'s Turn");
							System.out.println(brd);
			
								if (brd.playATurn() == true) {
									System.out.println("");
									System.out.println(player2+" Wins");
									winner2 = true;
									gameOver = true;
								}
								else {
				
									System.out.println("=-=-=-=-=-=-=-=");
				
									System.out.println(player2+"'s Turn");
									System.out.println(brd);
								}
			
			
								if (gameOver != true) {
				
								if(brd.playATurn() == true) {
									System.out.println("");
									System.out.println(player1+" Wins");
									winner1 = true;
									gameOver = true;
								}
				
							}
						}
						if (winner1 == true){
							wins1++;
							System.out.println("Would you like to play again?");
							ans = sc.next().charAt(0);
							winner1 = false;
						}
						if (winner2 == true){
							wins2++;
							System.out.println("Would you like to play again?");
							ans = sc.next().charAt(0);
							winner2 = false;
						}
					}
					System.out.println(player1+":"+wins1);
					System.out.println(player2+":"+wins2);
				}
			}

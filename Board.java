public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	public Board(){
		
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
	
	}
	
	public String toString(){
		String display = "";
		for(int i = 0;i < closedTiles.length;i++){
			
			if(closedTiles[i] == false){
				i++;
				display = display.concat(""+i+" ");
				i--;
			}
			else{
				display = display.concat("X"+" ");
			}
		}
		return display;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		int sum = die1.getPips() + die2.getPips();
		
		if(this.closedTiles[sum-1] == false){
			this.closedTiles[sum-1] = true;
			System.out.println("Closing Tile:"+ sum);
			return false;
		}
		else{
			return true;
		}
	}
}